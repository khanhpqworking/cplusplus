#include "GeneralHeader.h"

class Solution
{
public:
	ListNode* removeElements(ListNode* head, int val)
	{
		if (head == nullptr) return head;
		ListNode* throwList = head;

		ListNode* preNode = nullptr;
		ListNode* nextNode = nullptr;
		while (throwList != nullptr)
		{
			nextNode = throwList->next;

			if (throwList->val == val)
			{
				if (preNode)
				{
					preNode->next = nextNode;
				}

				delete throwList;
				throwList = nullptr;
			}
			else
			{
				if (!preNode)
				{
					preNode = throwList;
					head = preNode;
				}
				else preNode = throwList;
			}

			throwList = nextNode;
		}

		if (!preNode) return nullptr;

		return head;
	}
};

ListNode* CreateDataSampleRemoveLinkedElement()
{
	ListNode* head;
	ListNode* one   = nullptr;
	ListNode* two   = nullptr;
	ListNode* six_1 = nullptr;
	ListNode* three = nullptr;
	ListNode* four  = nullptr;
	ListNode* five  = nullptr;
	ListNode* six_2 = nullptr;

	six_2 = new ListNode(6);
	five = new ListNode(5, six_2);
	four = new ListNode(4, five);
	three = new ListNode(3, four);
	six_1 = new ListNode(6, three);
	two = new ListNode(2, six_1);
	one = new ListNode(1, two);

	head = one;

	return head;
}

//int main()
//{
//	Solution so;
//
//	ListNode* list = CreateDataSampleRemoveLinkedElement();
//	int val = 6;
//
//	ListNode* removedList = so.removeElements(list, val);
//
//	return 0;
//}