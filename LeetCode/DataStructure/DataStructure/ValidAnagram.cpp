#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	bool isAnagram(string s, string t)
	{
		if (s.size() != t.size()) return false;
		std::unordered_map<char, int> mapS;
		for (auto ch : s)
		{
			mapS[ch]++;
		}

		for (auto ch : t)
		{
			if (mapS.find(ch) != mapS.end())
			{
				mapS[ch]--;
				if (mapS[ch] == 0) mapS.erase(ch);
			}
		}

		if (mapS.size() == 0) return true;
		return false;
	}
};

//int main()
//{
//	Solution so;
//	std::string s = "rat";
//	std::string t = "car";
//
//	bool result = so.isAnagram(s, t);
//
//	return 0;
//}