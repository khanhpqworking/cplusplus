#include "GeneralHeader.h"
#include <map>

class Solution
{
public:
	bool containsDuplicate(vector<int>& nums)
	{
		map<int, bool> m;

		for (int i = 0; i < nums.size(); i++)
		{
			if (m[nums[i]] == true)
				return true;
			else
				m[nums[i]] = true;
		}
		return false;
	}
};

//int main()
//{
//	Solution so;
//	vector<int> v = { 2, 3, 7, 8, 7, 1, 2 };
//
//	bool isContainsDuplicate = so.containsDuplicate(v);
//
//	return 0;
//}

