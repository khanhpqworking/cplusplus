#include "GeneralHeader.h"
#include <map>

class Solution
{
public:
	vector<int> twoSum(vector<int>& nums, int target)
	{
		std::map<int, int> indexMap;// = new std::map<int>();
		for (int i = 0; i< nums.size(); i++)
		{
			int tmp = target - nums[i];
			auto result = indexMap.find(tmp);
			if (result != indexMap.end()) 
				return std::vector<int>{ i, result->second};
			else indexMap.insert({ nums[i], i });
		}
		return vector<int>();
	}
};

//int main()
//{
//	Solution so;
//	vector<int> v = { 3,3 };
//
//	vector<int> result = so.twoSum(v, 6);
//
//	return 0;
//}