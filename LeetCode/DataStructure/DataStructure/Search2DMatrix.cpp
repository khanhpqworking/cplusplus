#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	bool binarySearch(std::vector<int>& matrixRow, int begin, int end, int target)
	{
		if (begin == end) return (matrixRow[end] == target);
		int mid = begin + (end - begin) / 2;
		if (matrixRow[mid] == target) return true;
		else if (matrixRow[mid] > target)
		{
			if (mid - 1 < begin) return binarySearch(matrixRow, begin, begin, target);
			return binarySearch(matrixRow, begin, mid - 1, target);
		}
		else return binarySearch(matrixRow, mid + 1, end, target);
	}

	bool binarySearchMatrixRow(std::vector<int>& matrixRow, int target)
	{
		int mid = matrixRow.size() / 2;
		if (matrixRow[mid] == target) return true;
		else if (matrixRow[mid] > target)
		{
			if (mid-1 < 0) return binarySearch(matrixRow, 0, 0, target);
			return binarySearch(matrixRow, 0, mid - 1, target);
		}
		else
		{
			if (mid + 1 == matrixRow.size()) return binarySearch(matrixRow, mid, mid, target);
			return binarySearch(matrixRow, mid + 1, matrixRow.size() - 1, target);
		}
	}

	bool binarySearchMatrix(std::vector<std::vector<int>>& matrix, int begin, int end, int target)
	{
		if (begin == end) return binarySearchMatrixRow(matrix[end], target);

		int mid = begin + (end - begin) / 2;
		int borderCheck = matrix[mid][matrix[mid].size() - 1];
		if (borderCheck == target) return true;
		else if (borderCheck > target) return binarySearchMatrix(matrix, begin, mid, target);
		else return binarySearchMatrix(matrix, mid + 1, end, target);
	}

	bool searchMatrix(std::vector<std::vector<int>>& matrix, int target)
	{
		int size = matrix.size();
		int midMatrix = size / 2;
		
		int borderCheck = matrix[midMatrix][matrix[midMatrix].size() - 1];
		if (borderCheck == target) return true;
		else if (borderCheck > target) return binarySearchMatrix(matrix, 0, midMatrix, target);
		else
		{
			if (midMatrix + 1 == size) return binarySearchMatrix(matrix, midMatrix, midMatrix, target);
			return binarySearchMatrix(matrix, midMatrix + 1, size - 1, target);
		}
	}
};

//int main()
//{
//	Solution so;
//	//std::vector<std::vector<int>> matrix = { { 1,3,5,7 },{ 10,11,16,20 },{ 23,30,34,50 }};
//	//std::vector<std::vector<int>> matrix = { { 1 } };
//	std::vector<std::vector<int>> matrix = { { -9,-7,-5,-4,-4,-2 },{ -1,-1,-1,-1,-1,1 },{ 2,2,4,4,5,7 },{ 10,10,12,12,12,12 },{ 15,16,18,18,20,20 } };
//	int target = 19;
//
//	bool result = so.searchMatrix(matrix, target);
//
//	return 0;
//}