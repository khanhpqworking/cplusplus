#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	ListNode* deleteDuplicates(ListNode* head)
	{
		if (!head) return head;
		std::unordered_map<int, int> mapCheckDuplicate;

		ListNode* throwList = head;
		ListNode* prevNode = nullptr;

		while (throwList != nullptr)
		{
			ListNode* nextNode = throwList->next;

			if (mapCheckDuplicate.find(throwList->val) != mapCheckDuplicate.end())
			{
				prevNode->next = nextNode;
				delete throwList;
				throwList = nullptr;
			}
			else
			{
				mapCheckDuplicate[throwList->val]++;
				prevNode = throwList;
			}

			throwList = nextNode;
		}

		return head;
	}
};

ListNode* CreateDataSampleElementRemoveDuplicate()
{
	ListNode* head;
	ListNode* one = nullptr;
	ListNode* two = nullptr;
	ListNode* three = nullptr;
	ListNode* four = nullptr;
	ListNode* five = nullptr;

	five = new ListNode(3);
	four = new ListNode(3, five);
	three = new ListNode(2, four);
	two = new ListNode(1, three);
	one = new ListNode(1, two);

	head = one;

	return head;
}

int main()
{
	Solution so;

	ListNode* head = CreateDataSampleElementRemoveDuplicate();

	ListNode* result = so.deleteDuplicates(head);

	return 0;
}