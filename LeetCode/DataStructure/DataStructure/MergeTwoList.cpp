#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	ListNode* mergeTwoLists(ListNode* list1, ListNode* list2)
	{
		ListNode* head1 = list1;
		ListNode* head2 = list2;
		ListNode* listMerged = nullptr;
		while (head1 != nullptr && head2 != nullptr)
		{
			if (head1->val < head2->val)
			{
				ListNode* newNode = new ListNode(head1->val);
				head1 = head1->next;
			}
			else
			{
				head2 = head2->next;
			}
		}

		return listMerged;
	}
};

int main()
{
	Solution so;

	ListNode* list1 = CreateDataSample1();
	ListNode* list2 = CreateDataSample2();

	ListNode* result = so.mergeTwoLists(list1, list2);
	return 0;
}