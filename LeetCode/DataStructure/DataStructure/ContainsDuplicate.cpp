#include "GeneralHeader.h"

class Solution {
public:
	bool containsDuplicate(vector<int>& numsArray)
	{
		MergeSort(numsArray, 0, numsArray.size() - 1);
		for (int i = 0; i < numsArray.size() - 1; i++)
		{
			if (numsArray[i] == numsArray[i + 1]) return true;
		}
		return false;
	}

	void MergeSort(vector<int>& numsArray, const int begin, int end)
	{
		if (begin >= end) return;
		int mid = begin + (end - begin) / 2;
		MergeSort(numsArray, begin, mid);
		MergeSort(numsArray, mid + 1, end);
		Merge(numsArray, begin, mid, end);
	}

	void Merge(vector<int>& numsArray, const int left, const int mid, const int right)
	{
		const int numberElementLeftArray = mid - left + 1;
		const int numberElementRightArray = right - mid;

		int* leftArray = new int[numberElementLeftArray];
		int* rightARray = new int[numberElementRightArray];

		int indexLeftArray = 0, indexRightArray = 0;
		int indexMergeArray = left;

		for (int i = 0; i < numberElementLeftArray; i++)
		{
			leftArray[i] = numsArray[left + i];
		}

		for (int i = 0; i < numberElementRightArray; i++)
		{
			rightARray[i] = numsArray[mid + 1 + i];
		}

		while (indexLeftArray < numberElementLeftArray
			&& indexRightArray < numberElementRightArray)
		{
			if (leftArray[indexLeftArray] > rightARray[indexRightArray])
			{
				numsArray[indexMergeArray] = leftArray[indexLeftArray];
				indexLeftArray++;
			}
			else
			{
				numsArray[indexMergeArray] = rightARray[indexRightArray];
				indexRightArray++;
			}
			indexMergeArray++;
		}

		// Copy the remain element of left array to merged array
		while (indexLeftArray < numberElementLeftArray)
		{
			numsArray[indexMergeArray] = leftArray[indexLeftArray];
			indexLeftArray++;
			indexMergeArray++;
		}

		// Copy the remain element of right array to merged array
		while (indexRightArray < numberElementRightArray)
		{
			numsArray[indexMergeArray] = rightARray[indexRightArray];
			indexRightArray++;
			indexMergeArray++;
		}
	}
};

//int main()
//{
//	Solution so;
//	vector<int> v = { 2, 3, 7, 8, 7, 1, 2 };
//
//	bool isContainsDuplicate = so.containsDuplicate(v);
//
//	return 0;
//}

