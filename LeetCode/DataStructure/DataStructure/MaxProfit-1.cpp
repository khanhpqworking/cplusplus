#include "GeneralHeader.h"

class Solution
{
public:
	int maxProfit(vector<int>& prices)
	{
		int maxProfit = 0;
		int buy = INT_MAX;
		for (int i = 0; i < prices.size(); i++)
		{
			if (prices[i] < buy) buy = prices[i];
			int currentProfit = prices[i] - buy;
			if (currentProfit > maxProfit) maxProfit = currentProfit;
		}
		return maxProfit;
	}
};

//int main()
//{
//	Solution so;
//	std::vector<int> prices = { 7,6,4,3,1 };
//	int result = so.maxProfit(prices);
//	return 0;
//}