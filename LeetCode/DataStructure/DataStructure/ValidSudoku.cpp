#include "GeneralHeader.h"
#include <unordered_map>

class Solution {
public:
	bool isValidRow(std::vector<std::vector<char>>& board)
	{
		bool isValid = true;
		
		for (int i = 0; i < 9; i++)
		{
			std::unordered_map<char, int> mapItemRow;
			int column = 0;
			while (column < 9)
			{
				if (mapItemRow.find(board[i][column]) != mapItemRow.end() && board[i][column] != '.')
				{
					return false;
				}
				else
				{
					mapItemRow[board[i][column]]++;
				}
				column++;
			}
		}

		return isValid;
	}

	bool isValidCol(std::vector<std::vector<char>>& board)
	{
		bool isValid = true;

		for (int i = 0; i < 9; i++)
		{
			std::unordered_map<char, int> mapItemColumn;
			int row = 0;
			while (row < 9)
			{
				if (mapItemColumn.find(board[row][i]) != mapItemColumn.end() && board[row][i] != '.')
				{
					return false;
				}
				else
				{
					mapItemColumn[board[row][i]]++;
				}
				row++;
			}
		}

		return isValid;
	}

	bool isValidSmallSquare(std::vector<std::vector<char>>& board)
	{
		bool isValid = true;

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)// every j is 3 row of board
			{
				std::unordered_map<char, int> mapCheck;
				for (int k = 0; k < 3; k++)
				{
					for (int m = 0; m < 3; m++)
					{
						if (mapCheck.find(board[k+i*3][m+j*3]) != mapCheck.end() && board[k + i * 3][m + j * 3] != '.')
						{
							return false;
						}
						else
						{
							mapCheck[board[k + i * 3][m + j * 3]]++;
						}
					}
				}
			}
		}

		return isValid;
	}

	bool isValidSudoku(std::vector<std::vector<char>>& board)
	{
		return isValidRow(board) && isValidCol(board) && isValidSmallSquare(board);
	}
};

//int main()
//{
//	Solution so;
//	std::vector<std::vector<char>> board = {
//	 { '5','3','.','.','7','.','.','.','.' }
//	,{ '6','.','.','1','9','5','.','.','.' }
//	,{ '.','9','8','.','.','.','.','6','.' }
//	,{ '8','.','.','.','6','.','.','.','3' }
//	,{ '4','.','.','8','.','3','.','.','1' }
//	,{ '7','.','.','.','2','.','.','.','6' }
//	,{ '.','6','.','.','.','.','2','8','.' }
//	,{ '.','.','.','4','1','9','.','.','5' }
//	,{ '.','.','.','.','8','.','.','7','9' }
//	};
//
//	bool isValid = so.isValidSudoku(board);
//
//	return 0;
//}