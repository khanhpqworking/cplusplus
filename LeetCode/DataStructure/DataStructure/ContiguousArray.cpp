#include "GeneralHeader.h"

class Solution {
public:
	int maxSubArray(vector<int>& nums)
	{
		int max = INT_MIN;
		int maxCurrent = 0;
		for (int i = 0; i < nums.size(); i++)
		{
			if (nums[i] > max)
			{
				max = nums[i];
				maxCurrent = max;
			}

			maxCurrent += nums[i];
			if (maxCurrent > max) max = maxCurrent;

			maxCurrent = max;
		}
		return max;
	}
};

//int main()
//{
//	Solution so;
//	vector<int> v = { -2, 1, -3, 0 , -1, 2, 1, -5, 4 };
//
//	int maxSub = so.maxSubArray(v);
//
//	return 0;
//}