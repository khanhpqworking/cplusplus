#include "GeneralHeader.h"

class Solution
{
public:
	ListNode* mergeTwoLists(ListNode* list1, ListNode* list2)
	{
		if (list1 == nullptr && list2 != nullptr) return list2;
		if (list2 == nullptr && list1 != nullptr) return list1;

		ListNode* throwList1 = list1;
		ListNode* throwList2 = list2;
		ListNode* headMergedList = nullptr;
		ListNode* throwListMerge = nullptr;

		int index = 0;
		while (throwList1 != nullptr && throwList2 != nullptr)
		{
			if (throwList1->val > throwList2->val)
			{
				if(index == 0) throwListMerge = throwList2;
				else
				{
					throwListMerge->next = throwList2;
					throwListMerge = throwList2;
				}

 				throwList2 = throwList2->next;
			}
			else
			{
				if (index == 0) throwListMerge = throwList1;
				else
				{
					throwListMerge->next = throwList1;
					throwListMerge = throwList1;
				}

				throwList1 = throwList1->next;
			}

			if (index == 0) headMergedList = throwListMerge;
			
			index++;
		}

		if (throwList1 == nullptr && throwListMerge != nullptr && throwList2 != nullptr)
		{
			throwListMerge->next = throwList2;
		}
		if (throwList2 == nullptr && throwListMerge != nullptr && throwList1 != nullptr)
		{
			throwListMerge->next = throwList1;
		}

		return headMergedList;
	}

	ListNode* CreateDataSample1()
	{
		ListNode* head;
		ListNode* one = nullptr;
		ListNode* two = nullptr;
		ListNode* four = nullptr;

		one = new ListNode(1);
		two = new ListNode(2);
		four = new ListNode(4);

		one->next = two;
		two->next = four;

		head = one;
		return head;
	}

	ListNode* CreateDataSample2()
	{
		ListNode* head;
		ListNode* one = nullptr;
		ListNode* three = nullptr;
		ListNode* four = nullptr;

		one = new ListNode(1);
		three = new ListNode(3);
		four = new ListNode(4);

		one->next = three;
		three->next = four;

		head = one;
		return head;
	}
};

//int main()
//{
//	Solution so;
//
//	/*ListNode* list1 = CreateDataSample1();
//	ListNode* list2 = CreateDataSample2();*/
//
//	ListNode* list1 = nullptr;
//	ListNode* list2 = new ListNode(2);
//
//	ListNode* mergeList = so.mergeTwoLists(list1, list2);
//
//	return 0;
//}