#include "GeneralHeader.h"

class Solution {
public:
	int MaxInt(int a, int b)
	{
		return a > b ? a : b;
	}

	int MaxInt(int a, int b, int c)
	{
		return MaxInt(MaxInt(a, b), c);
	}

	int MaxSubCross(vector<int>& nums, int l, int m, int r)
	{
		int maxLeft = INT_MIN;
		int maxRight = INT_MIN;

		int maxItem = 0;
		for (int i = m; i >= l; i--)
		{
			maxItem += nums[i];
			if (maxItem > maxLeft) maxLeft = maxItem;
		}

		maxItem = 0;
		for (int i = m; i <= r; i++)
		{
			maxItem += nums[i];
			if (maxItem > maxRight) maxRight = maxItem;
		}

		return MaxInt(maxLeft + maxRight - nums[m], maxLeft, maxRight);
	}

	int MaxSub(vector<int>& nums, int l, int r)
	{
		if (l == r) return nums[l];

		int m = l + (r - l) / 2;

		int maxLeft = MaxSub(nums, l, m);
		int maxRight = MaxSub(nums, m + 1, r);

		int maxCross = MaxSubCross(nums, l, m, r);

		return MaxInt(maxCross, maxLeft, maxRight);
	}

	int maxSubArray(vector<int>& nums)
	{
		int result = MaxSub(nums, 0, nums.size() - 1);
		return result;
	}
};

//int main()
//{
//	Solution so;
//	vector<int> v = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
//
//	int maxSub = so.maxSubArray(v);
//
//	return 0;
//}

