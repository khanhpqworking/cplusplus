#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	vector<int> intersect(vector<int>& nums1, vector<int>& nums2)
	{
		unordered_map<int, int> mpp;
		vector<int> ans;

		for (auto it : nums1)
		{
			mpp[it]++;
		}

		for (auto it : nums2)
		{
			if (mpp[it]>0)
			{
				mpp[it]--;
				ans.push_back(it);
			}
		}
		return ans;

	}
};

//int main()
//{
//	Solution so;
//	std::vector<int> nums1 = { 1, 2, 2, 1 };
//	std::vector<int> nums2 = { 2, 2, 2 };
//
//	vector<int> result = so.intersect(nums1, nums2);
//
//	return 0;
//}
