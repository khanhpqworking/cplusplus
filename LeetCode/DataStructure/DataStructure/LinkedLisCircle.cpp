#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	bool hasCycle(ListNode *head)
	{
		if (!head) return false;

		std::unordered_map<long, int> mapAddress;
		while (head != NULL)
		{
			if (mapAddress.find((long)head->next) != mapAddress.end())
			{
				return true;
			}
			else
			{
				mapAddress[(long)head]++;
			}
			head = head->next;
		}

		return false;
	}
};

ListNode* CreateDataSample()
{
	ListNode* head;
	ListNode* one = NULL;
	ListNode* two = NULL;

	one = new ListNode(3);
	two = new ListNode(2);

	one->next = two;
	two->next = nullptr;

	head = one;
	return head;
}

//int main()
//{
//	Solution so;
//
//	Node* head = CreateDataSample();
//
//	bool result = so.hasCycle(head);
//	return 0;
//}