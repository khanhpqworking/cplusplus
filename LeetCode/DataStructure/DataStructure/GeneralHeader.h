#pragma once

#include <vector>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

struct ListNode
{
	int val;
	ListNode *next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode *next) : val(x), next(next) {}

	std::string valueString;
	
	std::string DisplayValueLinkedList(ListNode* head)
	{
		ListNode* throwList = head;
		std::string value = "";
		while (throwList != nullptr)
		{
			value.append(std::to_string(throwList->val)).append("; ");
			throwList = throwList->next;
		}

		return value;
	}

	std::string DisplayAddressLinkedList(ListNode* head)
	{
		ListNode* throwList = head;
		std::string addressString = "";
		while (throwList != nullptr)
		{
			const void * address = static_cast<const void*>(throwList);
			std::ostringstream ss;
			ss << address;

			std::string stringAddress = ss.str();

			addressString.append(stringAddress.append("; "));

			throwList = throwList->next;
		}

		return addressString;
	}
};
