#include "GeneralHeader.h"
#include <unordered_map>

class Solution
{
public:
	bool canConstruct(string ransomNote, string magazine)
	{
		std::unordered_map<char, int> mapRansomNote;
		if (magazine.size() < ransomNote.size()) return false;
		for (auto ch : ransomNote)
		{
			mapRansomNote[ch]++;
		}

		for (auto ch : magazine)
		{
			if (mapRansomNote.find(ch) != mapRansomNote.end())
			{
				mapRansomNote[ch]--;
				if (mapRansomNote[ch] == 0) mapRansomNote.erase(ch);
			}
		}

		if (mapRansomNote.size() == 0) return true;
		else return false;
	}
};

//int main()
//{
//	Solution so;
//	std::string ransomNote = "aa";
//	std::string magazine = "aab";
//
//	bool result = so.canConstruct(ransomNote, magazine);
//
//	return 0;
//}