#include "GeneralHeader.h"

void merge(vector<int>& nums1, int m, vector<int>& nums2, int n)
{
	if (m == 0) nums1 = nums2;
	if (n == 0) return;
	int indexLeft = m - 1, indexRight = n - 1;
	int i = (m + n) - 1;

	while (indexLeft >= 0 && indexRight >= 0)
	{
		if (nums1[indexLeft] < nums2[indexRight])
		{
			nums1[i] = nums2[indexRight];
			indexRight--;
		}
		else
		{
			nums1[i] = nums1[indexLeft];
			indexLeft--;
		}
		i--;
	}

	while (indexLeft >= 0 && i >= 0)
	{
		nums1[i] = nums1[indexLeft];
		indexLeft--;
		i--;
	}

	while (indexRight >= 0 && i >= 0)
	{
		nums1[i] = nums2[indexRight];
		indexRight--;
		i--;
	}
}


//int main()
//{
//	vector<int> v1 = { 1, 2, 3, 0, 0, 0 };
//	vector<int> v2 = { 2, 5, 6 };
//	int m = 3, n = 3;
//	merge(v1, m, v2, n);
//	return 0;
//}
