#include "GeneralHeader.h"

class Solution
{
public:
	ListNode* reverseList(ListNode* head)
	{
		if (!head) return head;

		ListNode* throwList = head;
		ListNode* preNode = nullptr;
		while (throwList != nullptr)
		{
			ListNode* nextNode = throwList->next;

			throwList->next = preNode;

			preNode = throwList;

			throwList = nextNode;
		}

		return preNode;
	}
};

ListNode* CreateDataSampleElement()
{
	ListNode* head;
	ListNode* one = nullptr;
	ListNode* two = nullptr;
	ListNode* three = nullptr;
	ListNode* four = nullptr;
	ListNode* five = nullptr;

	five = new ListNode(5);
	four = new ListNode(4, five);
	three = new ListNode(3, four);
	two = new ListNode(2, three);
	one = new ListNode(1, two);

	head = one;

	return head;
}

//int main()
//{
//	Solution so;
//
//	ListNode* head = CreateDataSampleElement();
//
//	ListNode* reverseList = so.reverseList(head);
//
//	return 0;
//}