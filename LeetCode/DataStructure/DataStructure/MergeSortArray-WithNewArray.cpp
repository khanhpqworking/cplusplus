#include "GeneralHeader.h"
class Solution {
public:
	void merge(vector<int>& nums1, int m, vector<int>& nums2, int n)
	{
		int index1 = 0, index2 = 0, mainIndex = 0;
		int *arrayLeft = nullptr, *arrayRight = nullptr;
		if (m > 0)
		{
			arrayLeft = new int[m];
			for (int i = 0; i < m; i++)
			{
				arrayLeft[i] = nums1[i];
			}
		}
		if (n > 0)
		{
			arrayRight = new int[n];
			for (int i = 0; i < n; i++)
			{
				arrayRight[i] = nums2[i];
			}
		}
		while (index1 < m && index2 < n)
		{
			if (arrayLeft[index1] > arrayRight[index2])
			{
				nums1[mainIndex] = arrayRight[index2];
				index2++;
			}
			else
			{
				nums1[mainIndex] = arrayLeft[index1];
				index1++;
			}
			mainIndex++;
		}

		if (index1 == m)
		{
			while (index2 < n)
			{
				nums1[mainIndex] = arrayRight[index2];
				index2++;
				mainIndex++;
			}
		}

		if (index2 == n)
		{
			while (index1 < m)
			{
				nums1[mainIndex] = arrayLeft[index1];
				index1++;
				mainIndex++;
			}
		}
	}
};

//int main()
//{
//	vector<int> v1 = { 1, 2, 3, 0, 0, 0 };
//	vector<int> v2 = { 2, 5, 6 };
//	int m = 3, n = 3;
//	merge(v1, m, v2, n);
//	return 0;
//}