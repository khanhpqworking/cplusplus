#include "GeneralHeader.h"
#include <unordered_map>


class Solution
{
public:
	int firstUniqChar(std::string s)
	{
		std::unordered_map<char, int> mapFreq;
		std::unordered_map<char, int> mapIndex;
		int result = INT_MAX;
		int index = 0;
		for (auto ch : s)
		{
			mapFreq[ch]++;
			mapIndex.insert({ ch, index });
			index++;
		}

		for (auto it = mapFreq.begin(); it != mapFreq.end(); it++)
		{
			if (it->second == 1)
			{
				int index = mapIndex.find(it->first)->second;
				if(index < result) result = index;
			}
		}

		if (result == INT_MAX) return -1;
		else return result;
	}
};

//int main()
//{
//	Solution so;
//	std::string s = "aabb";
//
//	int result = so.firstUniqChar(s);
//
//	return 0;
//}