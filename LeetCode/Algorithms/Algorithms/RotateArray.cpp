#include "GeneralHeader.h"
#include <map>

class Solution
{
public:
	void rotate(vector<int>& nums, int k)
	{
		if (nums.size() == 1) return;
		std::map<int,int> numsSave;
		int size = nums.size();
		for (int i = 0; i < nums.size(); i++)
		{
			int index = (i + k <= size - 1 ? i + k : i + k - size);
			while (index > size - 1) index -= size;
			if (numsSave.find(i) != numsSave.end())
			{
				numsSave.insert({ index, nums[index] });
				nums[index] = numsSave[i];
			}
			else
			{
				numsSave.insert({ index, nums[index] });
				nums[index] = nums[i];
			}
		}
	}
};

//int main()
//{
//	std::vector<int> nums{ 1, 2 };
//	int k = 3;
//	Solution so;
//	so.rotate(nums, k);
//	return 0;
//}
