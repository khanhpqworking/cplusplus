#include "GeneralHeader.h"
#include <map>
#include <math.h>

class Solution
{
public:
	std::vector<int> sortedSquares(std::vector<int>& nums)
	{
		std::map<int, int> absSorted;
		for (int i = 0; i < nums.size(); i++)
		{
			//absSorted.insert({ abs(nums[i]), i });
			absSorted[abs(nums[i])]++;
		}
		std::vector<int> result;
		for (std::map<int, int>::iterator it = absSorted.begin(); it != absSorted.end(); ++it)
		{
			for (int i = 0; i < it->second; i++)
			{
				result.push_back(pow(it->first, 2));
			}
		}
		return result;
	}
};

//int main()
//{
//	std::vector<int> nums{ -7,-3,2,3,11 };
//	Solution so;
//	std::vector<int> result = so.sortedSquares(nums);
//	return 0;
//}