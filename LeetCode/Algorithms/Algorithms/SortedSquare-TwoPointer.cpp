#include "GeneralHeader.h"

class Solution
{
public:
	std::vector<int> sortedSquares(std::vector<int>& nums)
	{
		int n = nums.size();

		vector<int> result(n);

		int left = 0;
		int right = n - 1;

		for (int i = n - 1; i >= 0; i--) {
			int square;
			if (abs(nums[left]) < abs(nums[right])) {
				square = nums[right];
				right--;
			}
			else {
				square = nums[left];
				left++;
			}
			result[i] = square * square;
		}
		return result;
	}
};

//int main()
//{
//	std::vector<int> nums{ -7,-3,2,3,11 };
//	Solution so;
//	std::vector<int> result = so.sortedSquares(nums);
//	return 0;
//}