#include "GeneralHeader.h"

class Solution
{
public:
	int binarySearch(::vector<int>& nums, int begin, int end, int target)
	{
		if (begin == end)
		{
			if (nums[begin] == target) return begin;
			else if (nums[begin] < target) return (begin + 1);
			else return (begin - 1 < 0 ? begin : 0);
		}
		int size = nums.size();
		int mid = begin + (end - begin) / 2;
		
		if (nums[mid] == target) return mid;
		else if (nums[mid] > target)
		{
			if (mid == 0) return 0;
			if (nums[mid - 1] < target) return mid;
			return binarySearch(nums, begin, mid - 1, target);
		}
		else
		{
			if (mid == size-1) return size;
			if (nums[mid + 1] > target) return mid + 1;
			return binarySearch(nums, mid + 1, end, target);
		}
	}

	int searchInsert(std::vector<int>& nums, int target)
	{
		return binarySearch(nums, 0, nums.size() - 1, target);
	}
};

//int main()
//{
//	std::vector<int> nums{ 1,3,5,6 };
//	int target = 0;
//	Solution so;
//	int result = so.searchInsert(nums, target);
//	return 0;
//}