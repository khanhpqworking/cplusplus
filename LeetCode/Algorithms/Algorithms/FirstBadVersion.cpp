class Solution
{
public:
	bool isBadVersion(int version)
	{
		if (version >= 4) return true;
		else return false;
	}

	int findFirstBadVersion(int left, int right)
	{
		if (right - 1 == left)
		{
			if (isBadVersion(left) && isBadVersion(right)) return left;
			else if (isBadVersion(right) && !isBadVersion(left)) return right;
			else return (right + 1);
		}
		
		int mid = left + (right - left) / 2;
		if (isBadVersion(mid) && !isBadVersion(mid - 1)) return mid;
		if (isBadVersion(mid) && isBadVersion(mid - 1))
		{
			return findFirstBadVersion(left, mid - 2);
		}
		else return findFirstBadVersion(mid + 1, right);

	}

	int firstBadVersion(int n)
	{
		return findFirstBadVersion(1, n);
	}
};

//int main()
//{
//	Solution so;
//	int n = 2147483647;
//	int result = so.firstBadVersion(n);
//	return 0;
//}

