#include "GeneralHeader.h"

class BinarySearch
{
public:
	int search(vector<int>& nums, int target)
	{
		return searchBinary(nums, 0, nums.size() - 1, target);
	}

	int searchBinary(vector<int>& nums, int begin, int end, int target)
	{
		if (begin == end) return (nums[begin] == target ? begin : -1);

		int mid = begin + (end - begin) / 2;

		if (nums[mid] == target) return mid;

		if (nums[mid] > target)
		{
			if (mid == 0) mid = 1;
			return searchBinary(nums, begin, mid - 1, target);
		}
		else return searchBinary(nums, mid + 1, end, target);
	}
};

//int main()
//{
//	BinarySearch so;
//	vector<int> nums = { -1,0,3,5,9,12 };
//	int target = 9;
//	int result = so.search(nums, target);
//	return 0;
//}

